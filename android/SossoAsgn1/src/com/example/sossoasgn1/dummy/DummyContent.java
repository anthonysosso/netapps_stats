package com.example.sossoasgn1.dummy;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DummyContent {

	public static class DummyItem {

		public String id;
		public String member_name, ranking, goals, assists, pts, plus_minus,
				pim, ppg, shg, hits, blocked_shots, shots, shot_percentage,
				gaa, goals_against, saves, save_percentage, shutouts;

		public DummyItem(String id, String member_name) {
			this.id = id;
			this.member_name = member_name;
		}

		public DummyItem(String member_name, String ranking, String goals,
				String assists, String pts, String plus_minus, String pim,
				String ppg, String shg, String hits, String blocked_shots,
				String shots, String shot_percentage, String gaa,
				String goals_against, String saves, String save_percentage,
				String shutouts) {
			this.member_name = member_name;
			this.ranking = ranking;
			this.goals = goals;
			this.assists = assists;
			this.pts = pts;
			this.plus_minus = plus_minus;
			this.pim = pim;
			this.ppg = ppg;
			this.shg = shg;
			this.hits = hits;
			this.blocked_shots = blocked_shots;
			this.shots = shots;
			this.shot_percentage = shot_percentage;
			this.gaa = gaa;
			this.goals_against = goals_against;
			this.saves = saves;
			this.save_percentage = save_percentage;
			this.shutouts = shutouts;
		}

		@Override
		public String toString() {
			return this.member_name + "\n" + this.shots;
		}
	}

	public static List<DummyItem> ITEMS = new ArrayList<DummyItem>();
	public static Map<String, DummyItem> ITEM_MAP = new HashMap<String, DummyItem>();

	static {
		addItem(new DummyItem("1", "Captain", "1", "Captain", "1", "Captain",
				"1", "Captain", "1", "Captain", "1", "Captain", "1", "Captain",
				"1", "Captain", "1", "Captain"));
		addItem(new DummyItem("2", "Item 2"));
		addItem(new DummyItem("3", "Item 3"));
	}

	private static void addItem(DummyItem item) {
		ITEMS.add(item);
		ITEM_MAP.put(item.id, item);
	}
}
