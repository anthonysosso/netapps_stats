package com.example.sossoasgn1.dummy;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.os.AsyncTask;

public class JsonFetchTask extends AsyncTask<Void, Void, ArrayList<String>> {

	@SuppressWarnings("unused")
	@Override
	protected ArrayList<String> doInBackground(Void... params) {
		ArrayList<String> listItems = new ArrayList<String>();
		try {
			URL webserviceJsonUrl = new URL(
					"https://dl.dropbox.com/u/6996716/mock.json");
			URLConnection tc = webserviceJsonUrl.openConnection();
			BufferedReader in = new BufferedReader(new InputStreamReader(
					tc.getInputStream()));
			String line;
			while ((line = in.readLine()) != null) {
				JSONArray ja = new JSONArray(line);
				for (int i = 0; i < ja.length(); i++) {
					JSONObject team_object = (JSONObject) ja.get(i);
					String club_abbreviation = team_object
							.getString("abbreviation");
					JSONObject players = team_object
							.getJSONObject("player_stats");
					String key;
					while ((key = (String) players.keys().next()) != null) {
						String value = (String) players.get(key);
						value = value;
					}
					@SuppressWarnings("unused")
					int x = 0;
				}
			}
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return listItems;
	}

	@Override
	protected void onPostExecute(ArrayList<String> result) {
		return;
	}
}
