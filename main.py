from Queue import Queue
from bs4 import BeautifulSoup
from google.appengine.api import urlfetch
from player import Player
from threading import Thread
import gspread
import urllib2
# or if your're using BeautifulSoup4:
# from bs4 import BeautifulSoup

#soup = BeautifulSoup(urllib2.urlopen('http://www.easportsworld.com/en_US/clubs/partial/501A0001/165794/members-list').read())
#soup = BeautifulSoup(urllib2.urlopen('https://dl.dropbox.com/u/6996716/EA%20SPORTS%20World.htm').read())

def update_stats():
    resp = urlfetch.fetch('http://www.easportsworld.com/en_US/clubs/partial/501A0001/165794/members-list', method=urlfetch.GET, deadline=20)
    ip = resp.content
    soup = BeautifulSoup(ip)
    players = []
    for row in soup('table', {'class' : 'styled full-width no-margin'})[0].tbody('tr'):
        tds = row('td')
        values = row.text.split('\n')
        player = Player(values)
        players.append(player)
    g = gspread.login('xtremerunnerars@gmail.com', 'yvncjbdbviqfxgwh')
    worksheet = g.open('Just The Tip Hockey').get_worksheet(7)
    for player_index, player in enumerate(players):
        cell_string = 'A' + str(player_index + 1 + 5) + ':S' + str(player_index + 1 + 5)
        cell_list = worksheet.range(cell_string)
        fields = player.fields_in_array_format()
        for index, cell in enumerate(cell_list):
            cell.value = fields[index]
        worksheet.update_cells(cell_list)

def get_stats(pgf=True, rdt=True, hrp=True):
    enclosure_queue = Queue()
    output_list = []
    if pgf:
        enclosure_queue.put({'abbreviation': 'pgf',
                             'url':'http://www.easportsworld.com/en_US/clubs/partial/501A0001/2755/members-list'})
    if rdt:
        enclosure_queue.put({'abbreviation': 'rdt',
                             'url':"http://www.easportsworld.com/en_US/clubs/partial/502A0001/9201/members-list"})
    if hrp:
        enclosure_queue.put({'abbreviation': 'hrp',
                             'url':"http://www.easportsworld.com/en_US/clubs/partial/501A0001/6030/members-list"})
    for i in range(3):
        worker = Thread(target=fetch_stats, args=(enclosure_queue, output_list))
        worker.setDaemon(False)
        worker.start()
    enclosure_queue.join()
    return output_list

#        fields = player.fields_in_array_format()

def fetch_stats(enclosure_queue, output_list):
    while True:
        info = enclosure_queue.get()
        resp = urlfetch.fetch(info['url'], method=urlfetch.GET, deadline=20)
        ip = resp.content
        soup = BeautifulSoup(ip)
        players = []
        for row in soup('table', {'class' : 'styled full-width no-margin'})[0].tbody('tr'):
            tds = row('td')
            values = row.text.split('\n')
            player = Player(values)
            players.append(player)
        info['player_stats'] = players
        output_list.append(info)
        enclosure_queue.task_done()
